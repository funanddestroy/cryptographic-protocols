﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Numerics;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Coin
{
    class Program
    {
        static string cpr = " > ";

        static string address = "127.0.0.1";
        static int port = 2200;
        static int bufferSize = 1000000;
        static Encoding encoding = Encoding.UTF8;
        static byte[] buffer = new byte[bufferSize];
        static Random rand = new Random();

        static void StartServer()
        {
            try
            {
                IPAddress localAddress = IPAddress.Parse(address);
                TcpListener listener = new TcpListener(localAddress, port);

                listener.Start(1);

                while (true)
                {
                    Console.Write(" Введите количесттво бит простого числа:\n" + cpr);
                    int bits = int.Parse(Console.ReadLine());

                    BigInteger p;
                    BigInteger q;

                    do
                    {
                        p = GetPrimeOverBits(bits);
                        q = (p - 1) / 2;
                    } while (!IsPrime(q));

                    Console.WriteLine(" p = " + p);

                    Console.WriteLine(" Сервер ожидает {0}", listener.LocalEndpoint);
                    TcpClient client = listener.AcceptTcpClient();
                    NetworkStream io = client.GetStream();

                    Console.WriteLine(" Принято соединение от {0}", client.Client.RemoteEndPoint);

                    SendString(io, p.ToString());

                    BigInteger h = BigInteger.Parse(ReadString(io));
                    Console.WriteLine(" Принято h = " + h);

                    if (BigInteger.ModPow(h, (p - 1) / q, p) != 1)
                    {
                        Console.WriteLine(" h - примитивный элемент");
                    }
                    else
                    {
                        Console.WriteLine(" h - не примитивный элемент");
                        continue;
                    } 

                    BigInteger t = BigInteger.Parse(ReadString(io));
                    Console.WriteLine(" Принято t = " + t);

                    if (BigInteger.ModPow(t, (p - 1) / q, p) != 1)
                    {
                        Console.WriteLine(" t - примитивный элемент");
                    }
                    else
                    {
                        Console.WriteLine(" t - не примитивный элемент");
                        continue;
                    }

                    string str = "";
                    for (int j = 0; j < p.ToString().Length; j++)
                        str += rand.Next() % 10;

                    BigInteger x = BigInteger.Parse(str) % p;

                    while (BigInteger.GreatestCommonDivisor(x, p - 1) != 1)
                    {
                        x = (x + 1) % (p - 1);
                        if (x < 2) x = 2;
                    }

                    Console.Write(" h или t?\n" + cpr);
                    string HorT = Console.ReadLine();

                    BigInteger y = 0;

                    if (HorT == "h")
                    {
                        y = BigInteger.ModPow(h, x, p);
                    }
                    else
                    {
                        y = BigInteger.ModPow(t, x, p);
                    }

                    Console.WriteLine(" y = " + y);
                    SendString(io, y.ToString());

                    string OhtR = ReadString(io);

                    string coin = "";

                    if (OhtR == HorT)
                    {
                        coin = "\tОРЕЛ";
                    }
                    else
                    {
                        coin = "\tРЕШКА";
                    }

                    Console.WriteLine(coin);
                    SendString(io, coin);

                    Console.WriteLine(" Отправка x..");
                    SendString(io, x.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" Произошла ошибка {0}", e.Message);
            }
        }

        static void StartClient()
        {
            try
            {
                TcpClient client = new TcpClient(address, port);

                NetworkStream io = client.GetStream();

                BigInteger p = BigInteger.Parse(ReadString(io));
                Console.WriteLine(" Принято p = " + p);

                BigInteger q = (p - 1) / 2;

                string str = "";
                for (int j = 0; j < p.ToString().Length; j++)
                    str += rand.Next() % 10;

                BigInteger h = BigInteger.Parse(str) % p;

                while (BigInteger.ModPow(h, (p - 1) / q, p) == 1)
                {
                    h++;
                }

                Console.WriteLine(" Выбранно h = " + h);
                SendString(io, h.ToString());

                str = "";
                for (int j = 0; j < p.ToString().Length; j++)
                    str += rand.Next() % 10;

                BigInteger t = BigInteger.Parse(str) % p;

                while (BigInteger.ModPow(t, (p - 1) / q, p) == 1)
                {
                    t++;
                }

                Console.WriteLine(" Выбранно t = " + t);
                SendString(io, t.ToString());

                BigInteger y = BigInteger.Parse(ReadString(io));
                Console.WriteLine(" Принято y = " + y);

                Console.Write(" h или t?\n" + cpr);
                string HorT = Console.ReadLine();

                SendString(io, HorT);

                string coin = ReadString(io);
                Console.WriteLine(coin);

                BigInteger x = BigInteger.Parse(ReadString(io));
                Console.WriteLine(" Принято x = " + x);

                if (BigInteger.GreatestCommonDivisor(x, p - 1) == 1)
                {
                    Console.WriteLine(" x и (p - 1) взаимно просты");
                }
                else
                {
                    Console.WriteLine(" x и (p - 1) НЕ взаимно просты");
                }

                Console.WriteLine(" y = h ^ x (mod p) = " + BigInteger.ModPow(h, x, p));
                Console.WriteLine(" y = t ^ x (mod p) = " + BigInteger.ModPow(t, x, p));
            }
            catch (Exception e)
            {
                Console.WriteLine(" Произошла ошибка {0}", e.Message);
            }
        }

        static string ReadString(NetworkStream io)
        {
            int length = io.Read(buffer, 0, buffer.Length);
            if (length == 0)
                return null;
            return encoding.GetString(buffer, 0, length);
        }

        static void SendString(NetworkStream io, string s)
        {
            var array = encoding.GetBytes(s);
            io.Write(array, 0, array.Length);
        }

        static BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        static BigInteger GetPrime(BigInteger n)
        {
            string str = "";
            for (int j = 0; j < n.ToString().Length; j++)
                str += rand.Next() % 10;

            BigInteger tmp = BigInteger.Parse(str) % n;
            if (tmp < 3) tmp = 3;
            if (tmp % 2 == 0) tmp++;

            while (!IsPrime(tmp))
            {
                tmp += 2;
            }
            
            return tmp;
        }

        static bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }



        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(" Запуск программы в режиме сервера или клиента? [s/c]:");
                Console.Write(cpr);
                string ans = Console.ReadLine();

                switch (ans)
                {
                    case "s":
                        StartServer();
                        break;
                    case "c":
                        StartClient();
                        break;
                    case "exit":
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
