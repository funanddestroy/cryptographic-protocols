﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySHA1
{
    class mySHA1
    {
        private static int SHA1HashSize = 20;

        private uint[] Intermediate_Hash = new uint[SHA1HashSize / 4]; /* Message Digest  */

        private uint Length_Low = 0;            /* Message length in bits      */
        private uint Length_High = 0;           /* Message length in bits      */

        /* Index into message block array   */
        private int Message_Block_Index = 0;
        private byte[] Message_Block = new byte[64];      /* 512-bit message blocks      */

        private bool Computed = false;               /* Is the digest computed?         */
        private bool Corrupted = false;             /* Is the message digest corrupted? */

        public byte[] ComputeSHA1(byte[] message)
        {
            this.SHA1Reset();
            this.SHA1Input(message);
            return this.SHA1Result();
        }

        private uint SHA1CircularShift(uint bits, uint word)
        {
            return (uint)((word << (int)bits) | (word >> 32 - (int)bits));
        }

        private void SHA1Reset()
        {
            Length_Low = 0;
            Length_High = 0;
            Message_Block_Index = 0;

            Intermediate_Hash[0] = 0x67452301;
            Intermediate_Hash[1] = 0xEFCDAB89;
            Intermediate_Hash[2] = 0x98BADCFE;
            Intermediate_Hash[3] = 0x10325476;
            Intermediate_Hash[4] = 0xC3D2E1F0;

            Computed = false;
            Corrupted = false;
        }

        private byte[] SHA1Result()
        {
            byte[] Message_Digest = new byte[SHA1HashSize];

            if (Corrupted)
            {
                throw new Exception();
            }

            if (!Computed)
            {
                SHA1PadMessage();
                for (int i = 0; i < 64; ++i)
                {
                    /* message may be sensitive, clear it out */
                    Message_Block[i] = 0;
                }
                Length_Low = 0;    /* and clear length */
                Length_High = 0;
                Computed = true;
            }

            for (int i = 0; i < SHA1HashSize; ++i)
            {
                Message_Digest[i] = (byte)(Intermediate_Hash[i >> 2] >> 8 * (3 - (i & 0x03)));
            }

            return Message_Digest;
        }

        private void SHA1Input(byte[] message)
        {
            if (Computed)
            {
                throw new Exception();
            }

            if (Corrupted)
            {
                throw new Exception();
            }

            int length = message.Length;
            int message_array = 0;

            while (length-- > 0 && !Corrupted)
            {
                Message_Block[Message_Block_Index++] = (byte)(message[message_array] & 0xFF);

                Length_Low += 8;
                if (Length_Low == 0)
                {
                    Length_High++;
                    if (Length_High == 0)
                    {
                        /* Message is too long */
                        Corrupted = true;
                    }
                }

                if (Message_Block_Index == 64)
                {
                    SHA1ProcessMessageBlock();
                }

                message_array++;
            }
        }

        private void SHA1ProcessMessageBlock()
        {
            uint[] K = { 0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xCA62C1D6 };
            
            uint temp;
            uint[] W = new uint[80];
            uint A, B, C, D, E;

            for (int t = 0; t < 16; t++)
            {
                W[t] = (uint)Message_Block[t * 4] << 24;
                W[t] |= (uint)Message_Block[t * 4 + 1] << 16;
                W[t] |= (uint)Message_Block[t * 4 + 2] << 8;
                W[t] |= (uint)Message_Block[t * 4 + 3];
            }

            for (int t = 16; t < 80; t++)
            {
                W[t] = SHA1CircularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);
            }

            A = Intermediate_Hash[0];
            B = Intermediate_Hash[1];
            C = Intermediate_Hash[2];
            D = Intermediate_Hash[3];
            E = Intermediate_Hash[4];

            for (int t = 0; t < 20; t++)
            {
                temp = SHA1CircularShift(5, A) + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
                E = D;
                D = C;
                C = SHA1CircularShift(30, B);
                B = A;
                A = temp;
            }

            for (int t = 20; t < 40; t++)
            {
                temp = SHA1CircularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[1];
                E = D;
                D = C;
                C = SHA1CircularShift(30, B);
                B = A;
                A = temp;
            }

            for (int t = 40; t < 60; t++)
            {
                temp = SHA1CircularShift(5, A) +
                       ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
                E = D;
                D = C;
                C = SHA1CircularShift(30, B);
                B = A;
                A = temp;
            }

            for (int t = 60; t < 80; t++)
            {
                temp = SHA1CircularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[3];
                E = D;
                D = C;
                C = SHA1CircularShift(30, B);
                B = A;
                A = temp;
            }

            Intermediate_Hash[0] += A;
            Intermediate_Hash[1] += B;
            Intermediate_Hash[2] += C;
            Intermediate_Hash[3] += D;
            Intermediate_Hash[4] += E;

            Message_Block_Index = 0;
        }

        private void SHA1PadMessage()
        {
            if (Message_Block_Index > 55)
            {
                Message_Block[Message_Block_Index++] = 0x80;
                while (Message_Block_Index < 64)
                {
                    Message_Block[Message_Block_Index++] = 0;
                }

                SHA1ProcessMessageBlock();

                while (Message_Block_Index < 56)
                {
                    Message_Block[Message_Block_Index++] = 0;
                }
            }
            else
            {
                Message_Block[Message_Block_Index++] = 0x80;
                while (Message_Block_Index < 56)
                {
                    Message_Block[Message_Block_Index++] = 0;
                }
            }

            Message_Block[56] = (byte)(Length_High >> 24);
            Message_Block[57] = (byte)(Length_High >> 16);
            Message_Block[58] = (byte)(Length_High >> 8);
            Message_Block[59] = (byte)(Length_High);
            Message_Block[60] = (byte)(Length_Low >> 24);
            Message_Block[61] = (byte)(Length_Low >> 16);
            Message_Block[62] = (byte)(Length_Low >> 8);
            Message_Block[63] = (byte)(Length_Low);
            
            SHA1ProcessMessageBlock();
        }

    }
}
