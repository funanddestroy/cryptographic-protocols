﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Numerics;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DIFFIE_HELLMAN
{
    class Program
    {
        static string cpr = " > ";

        static string address = "127.0.0.1";
        static int port = 2200;
        static int bufferSize = 1000000;
        static Encoding encoding = Encoding.UTF8;
        static byte[] buffer = new byte[bufferSize];
        static int N = 100;

        static BigInteger n = 1;
        static BigInteger g;

        static void StartServer()
        {
            Console.Write("   Введите битность простого числа n:\n" + cpr);
            int bits = int.Parse(Console.ReadLine());

            int b = bits / 2 + 1;

            BigInteger q;

            int i = CountBits(n);

            while (i != bits)
            {
                q = GetPrimeOverBits(b);
                g = GetG(ref n, q);

                i = CountBits(n);
            }

            Console.WriteLine("   n = " + n);
            Console.WriteLine("   g = " + g);

            try
            {
                IPAddress localAddress = IPAddress.Parse(address);
                TcpListener listener = new TcpListener(localAddress, port);

                listener.Start(1);

                while (true)
                {
                    Console.WriteLine("   Сервер ожидает {0}", listener.LocalEndpoint);
                    TcpClient client = listener.AcceptTcpClient();
                    NetworkStream io = client.GetStream();

                    Console.WriteLine("   Принято соединение от {0}", client.Client.RemoteEndPoint);

                    Console.WriteLine("   Отправка числа n...\n");
                    SendString(io, n.ToString());

                    Thread.Sleep(250);

                    Console.WriteLine("   Отправка числа g...\n");
                    SendString(io, g.ToString());

                    Thread.Sleep(250);

                    Random rand = new Random();
                    BigInteger x = 0;
                    string str = "";
                    for (int j = 0; j < n.ToString().Length; j++)
                        str += rand.Next() % 10;
                    x = BigInteger.Parse(str);
                    x %= n;

                    BigInteger X = BigInteger.ModPow(g, x, n);
                    Console.WriteLine("   Отправка числа X = " + X + "...\n");
                    SendString(io, X.ToString());

                    //Thread.Sleep(250);

                    BigInteger Y = BigInteger.Parse(ReadString(io));
                    Console.WriteLine("   Получено число Y: " + Y + "\n");

                    BigInteger k = BigInteger.ModPow(Y, x, n);
                    Console.WriteLine("   Общий ключ k = " + k);


                    Console.WriteLine("   Закрытие соединения");
                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("   Произошла ошибка {0}", e.Message);
            }
        }

        static void StartClient()
        {
            try
            {
                TcpClient client = new TcpClient(address, port);

                NetworkStream io = client.GetStream();

                n = BigInteger.Parse(ReadString(io));
                Console.WriteLine("   Получено число n: " + n);

                g = BigInteger.Parse(ReadString(io));
                Console.WriteLine("   Получено число g: " + g);

                BigInteger X = BigInteger.Parse(ReadString(io));
                Console.WriteLine("   Получено число X: " + X);

                Random rand = new Random();
                BigInteger y = 0;
                string str = "";
                for (int j = 0; j < n.ToString().Length; j++)
                    str += rand.Next() % 10;
                y = BigInteger.Parse(str);
                y %= n;

                BigInteger Y = BigInteger.ModPow(g, y, n);
                Console.WriteLine("\n   Отправка числа Y = " + Y + "...\n");
                SendString(io, Y.ToString());

                BigInteger k = BigInteger.ModPow(X, y, n);
                Console.WriteLine("   Общий ключ k' = " + k);

            }
            catch (Exception e)
            {
                Console.WriteLine("   Произошла ошибка {0}", e.Message);
            }
        }

        static string ReadString(NetworkStream io)
        {
            int length = io.Read(buffer, 0, buffer.Length);
            if (length == 0)
                return null;
            return encoding.GetString(buffer, 0, length);
        }

        static void SendString(NetworkStream io, string s)
        {
            var array = encoding.GetBytes(s);
            io.Write(array, 0, array.Length);
        }

        static BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        static BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        static bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        static BigInteger GetG(ref BigInteger p, BigInteger q)
        {
            BigInteger s = 0;

            BigInteger g = 1;

            if (!IsPrime(q))
            {
                throw new ArgumentException("Введено не простое число");
            }

            Random rand = new Random();
            s = 0;
            string str = "";
            for (int k = 0; k < q.ToString().Length; k++)
            {
                str += rand.Next() % 10;
            }

            s = BigInteger.Parse(str);
            s %= q;

            if (s < 2) { s = 2; }
            if (s > 2 && s % 2 != 0) { s -= 1; }

            BigInteger _s = s;
            do
            {
                p = q;
                p *= s;
                p += 1;

                _s = s;
                s += 2;

                s %= q;

                if (s < 2) { s = 2; }
            } while (!IsPrime(p));

            s = _s;

            BigInteger p1 = p - 1;
            BigInteger p2 = p - 2;

            BigInteger t = p1 / q;

            while (g == 1)
            {
                BigInteger a = 0;
                while (a > p2 || a < 2)
                {
                    str = "";
                    for (int k = 0; k < p1.ToString().Length; k++)
                    {
                        str += rand.Next() % 10;
                    }

                    a = BigInteger.Parse(str);
                }

                g = BigInteger.ModPow(a, t, p);
            }

            return g;
        }

        static int CountBits(BigInteger number)
        {
            int j = 8;
            byte bt = number.ToByteArray()[number.ToByteArray().Length - 1];
            while ((int)(bt & (1 << (j - 1))) == 0 && j != 0)
            {
                j--;
            }

            return (number.ToByteArray().Length - 1) * 8 + j;
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("   Запуск программы в режиме сервера или клиента? [s/c]:");
                Console.Write(cpr);
                string ans = Console.ReadLine();

                switch (ans)
                {
                    case "s":
                        StartServer();
                        break;
                    case "c":
                        StartClient();
                        break;
                    case "exit":
                        return;
                    default:
                        Console.WriteLine("   Неверный параметр.");
                        break;
                }
            }
        }
    }
}
