﻿using System;
using System.Numerics;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElGamal
{
    class ElGamal
    {
        private const int BASE = 10;
        private BigInteger ZERO = BigInteger.Zero;
        private BigInteger ONE = BigInteger.One;
        private BigInteger TWO = BigInteger.Parse("2");

        private BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        private bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % TWO == ZERO)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = ZERO;
                while (tmp > p1 || tmp < TWO)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % BASE;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= ONE || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, TWO, p);

                    if (x == ONE)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        private BigInteger GetG(ref BigInteger p, BigInteger q)
        {
            BigInteger s = ZERO;

            BigInteger g = ONE;

            if (!IsPrime(q))
            {
                throw new ArgumentException("Введено не простое число");
            }

            Random rand = new Random();
            s = ZERO;
            string str = "";
            for (int k = 0; k < q.ToString().Length; k++)
            {
                str += rand.Next() % BASE;
            }

            s = BigInteger.Parse(str);
            s %= q;

            if (s < TWO) { s = TWO; }
            if (s > TWO && s % TWO != ZERO) { s -= ONE; }

            BigInteger _s = s;
            do
            {
                p = q;
                p *= s;
                p += ONE;

                _s = s;
                s += TWO;

                s %= q;

                if (s < TWO) { s = TWO; }
            } while (!IsPrime(p));

            s = _s;

            BigInteger p1 = p - ONE;
            BigInteger p2 = p - TWO;

            BigInteger t = p1 / q;

            while (g == ONE)
            {
                BigInteger a = ZERO;
                while (a > p2 || a < TWO)
                {
                    str = "";
                    for (int k = 0; k < p1.ToString().Length; k++)
                    {
                        str += rand.Next() % BASE;
                    }

                    a = BigInteger.Parse(str);
                }

                g = BigInteger.ModPow(a, t, p);
            }

            return g;
        }

        private BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B) //if A less than B, switch them
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }

        private int CountBits(BigInteger number)
        {
            int j = 8;
            byte bt = number.ToByteArray()[number.ToByteArray().Length - 1];
            while ((int)(bt & (1 << (j - 1))) == 0 && j != 0)
            {
                j--;
            }

            return (number.ToByteArray().Length - 1) * 8 + j;
        }

        public void CreateKeys(int bits)
        {
            int b = bits / 2 + 1;

            BigInteger p = ONE;
            BigInteger q = ZERO;
            BigInteger g = ZERO;

            int i = CountBits(p);

            while (i != bits)
            {
                q = GetPrimeOverBits(b);
                g = GetG(ref p, q);                

                i = CountBits(p);
            }

            Console.WriteLine("p = " + p);
            Console.WriteLine("g = " + g);

            Random rand = new Random();
            BigInteger x = ZERO;
            string str = "";
            for (int k = 0; k < p.ToString().Length; k++)
            {
                str += rand.Next() % BASE;
            }
            x = BigInteger.Parse(str);
            x %= p;
            if (x < TWO) { x = TWO; }

            Console.WriteLine("x = " + x);

            BigInteger y = BigInteger.ModPow(g, x, p);

            Console.WriteLine("y = " + y);

            File.WriteAllText("PrivateKey", p + "\n" + g + "\n" + x);
            File.WriteAllText("PublicKey", p + "\n" + g + "\n" + y);
        }

        public void SignMessage(string file, string privateKey)
        {
            string text = File.ReadAllText(file);

            string[] lines = File.ReadAllLines(privateKey);

            BigInteger p = BigInteger.Parse(lines[0]);
            BigInteger g = BigInteger.Parse(lines[1]);
            BigInteger x = BigInteger.Parse(lines[2]);

            using (MD5 md5Hash = MD5.Create())
            {
                byte[] hash = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(text));

                BigInteger m = ((new BigInteger(hash)) % p + p) % p;

                BigInteger pm1 = p - 1;

                Random rand = new Random();
                BigInteger k = ZERO;
                string str = "";
                for (int i = 0; i < pm1.ToString().Length; i++)
                {
                    str += rand.Next() % BASE;
                }
                k = BigInteger.Parse(str);
                k %= pm1;
                if (k < TWO) { k = TWO; }
                BigInteger[] gcd = Extended_GCD(k, pm1);
                while (gcd[0] != 1)
                {
                    k = (k + 1) % pm1;
                    gcd = Extended_GCD(k, pm1);
                }

                BigInteger r = BigInteger.ModPow(g, k, p);
                Console.WriteLine("r = " + r);

                BigInteger s = (((m - x * r) * gcd[1]) % pm1 + pm1) % pm1;
                Console.WriteLine("s = " + s);

                File.WriteAllText(file + ".sign", r + "\n" + s);
            }
        }

        public bool TestSign(string file, string sign, string publicKey)
        {
            string text = File.ReadAllText(file);

            string[] signLines = File.ReadAllLines(sign);

            BigInteger r = BigInteger.Parse(signLines[0]);
            BigInteger s = BigInteger.Parse(signLines[1]);

            string[] lines = File.ReadAllLines(publicKey);

            BigInteger p = BigInteger.Parse(lines[0]);
            BigInteger g = BigInteger.Parse(lines[1]);
            BigInteger y = BigInteger.Parse(lines[2]);

            using (MD5 md5Hash = MD5.Create())
            {
                byte[] hash = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(text));

                BigInteger m = ((new BigInteger(hash)) % p + p) % p;

                if (r < 0 || r > p || s < 0 || s > p - 1)
                {
                    return false;
                }

                BigInteger a = (BigInteger.ModPow(y, r, p) * BigInteger.ModPow(r, s, p)) % p;
                Console.WriteLine("(y^r * r^s) mod p =\t" + a);
                BigInteger b = BigInteger.ModPow(g, m, p);
                Console.WriteLine("g^m mod p =\t\t" + b);

                if (a == b)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
