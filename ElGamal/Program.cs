﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElGamal
{
    class Program
    {
        static void Main(string[] args)
        {
            ElGamal elgamal = new ElGamal();

            string help = "genkeys\t- сгенерировать ключи;\n" +
                          "sign\t- подписать файл;\n" +
                          "test\t- проверить подпись;\n" +
                          "exit\t- выход из программы.\n" +
                          "\n";
            Console.Write(help);


            string pr = "> ";

            while (true)
            {
                Console.Write(pr);
                string command = Console.ReadLine();

                switch (command)
                {
                    case "genkeys":
                        Console.WriteLine("Введите количество бит: ");
                        Console.Write(pr);
                        int bits = int.Parse(Console.ReadLine());
                        elgamal.CreateKeys(bits);
                        break;

                    case "sign":
                        Console.WriteLine("Private key: ");
                        Console.Write(pr);
                        string key = Console.ReadLine();
                        Console.WriteLine("Введите имя файла: ");
                        Console.Write(pr);
                        string file = Console.ReadLine();
                        elgamal.SignMessage(file, key);
                        break;

                    case "test":
                        Console.WriteLine("Public key: ");
                        Console.Write(pr);
                        key = Console.ReadLine();
                        Console.WriteLine("Введите имя файла: ");
                        Console.Write(pr);
                        file = Console.ReadLine();
                        Console.WriteLine("Подпись: ");
                        Console.Write(pr);
                        string sign = Console.ReadLine();
                        if (elgamal.TestSign(file, sign, key))
                        {
                            Console.WriteLine("\nПодпись верна!");
                        }
                        else
                        {
                            Console.WriteLine("\nПодпись неверна!");
                        }
                        break;

                    case "exit":
                        return;
                }
            }
        }
    }
}
