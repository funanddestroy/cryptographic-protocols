﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Numerics;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SKey
{
    class Program
    {
        static string cpr = " > ";

        static string address = "127.0.0.1";
        static int port = 2200;
        static int bufferSize = 1000000;
        static Encoding encoding = Encoding.UTF8;
        static byte[] buffer = new byte[bufferSize];
        static int N = 100;
        static int randomNumberLen = 50;

        static void StartServer()
        {
            try
            {
                IPAddress localAddress = IPAddress.Parse(address);
                TcpListener listener = new TcpListener(localAddress, port);

                listener.Start(1);

                while (true)
                {
                    Console.WriteLine("   Сервер ожидает {0}", listener.LocalEndpoint);
                    TcpClient client = listener.AcceptTcpClient();
                    NetworkStream io = client.GetStream();

                    Console.WriteLine("   Принято соединение от {0}", client.Client.RemoteEndPoint);

                    string printMsg = "   Регистрация/Вход [r/l]:\n" + cpr;
                    SendString(io, printMsg);
                    Console.Write(printMsg);

                    string ans = ReadString(io);
                    Console.WriteLine(ans);
                    switch (ans)
                    {
                        case "r":
                            printMsg = "   Имя пользователя:\n" + cpr;
                            SendString(io, printMsg);
                            Console.Write(printMsg);

                            string userName = ReadString(io);
                            Console.WriteLine(userName);

                            BigInteger number = BigInteger.Parse(ReadString(io));
                            Console.WriteLine("   Число пользователя: " + number);

                            string rec = AddUser(userName, number);
                            SendString(io, rec);

                            break;

                        case "l":
                            printMsg = "   Имя пользователя:\n" + cpr;
                            SendString(io, printMsg);
                            Console.Write(printMsg);

                            userName = ReadString(io);
                            Console.WriteLine(userName);

                            if (!File.Exists(@".\Server\" + userName))
                            {
                                Console.WriteLine("   Вы не зарегистрированы!");
                                break;
                            }

                            //Thread.Sleep(1000);

                            printMsg = "   Пароль:\n" + cpr;
                            SendString(io, printMsg);
                            Console.Write(printMsg);

                            string userPassword = ReadString(io);
                            Console.WriteLine(userPassword);

                            string password = File.ReadAllText(@".\Server\" + userName);
                            //Console.WriteLine("   Сохраненный пароль: " + password);

                            BigInteger uP = BigInteger.Parse(userPassword);
                            BigInteger p = BigInteger.Parse(password);

                            using (MD5 md5Hash = MD5.Create())
                            {
                                byte[] hash = md5Hash.ComputeHash(ReOptMass(uP.ToByteArray()));
                                BigInteger h = new BigInteger(OptMass(hash));
                                if (p != h)
                                {
                                    Console.WriteLine("   Неверный пароль!");
                                    break;
                                }
                            }

                            File.WriteAllText(@".\Server\" + userName, userPassword);

                            SendString(io, "\n   Успешный вход в комнату ЭХО!!!\n   Чтоб выйти из этой комнаты введите 'exit'\n" + cpr);

                            string echo = "";
                            while (echo != "exit") {
                                echo = ReadString(io);
                                SendString(io, "   " + echo + "\n" + cpr);
                            }

                            break;
                        default:
                            continue;
                    }

                    Console.WriteLine("   Закрытие соединения");
                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("   Произошла ошибка {0}", e.Message);
            }
        }

        static void StartClient()
        {
            try
            {
                TcpClient client = new TcpClient(address, port);
                
                NetworkStream io = client.GetStream();                

                Console.Write(ReadString(io));
                string ans = Console.ReadLine();
                SendString(io, ans);

                switch (ans)
                {
                    case "r":
                        Console.Write(ReadString(io));
                        string userName = Console.ReadLine();
                        SendString(io, userName);

                        Random rand = new Random();
                        BigInteger number = 0;
                        string str = "";
                        for (int j = 0; j < randomNumberLen; j++)
                            str += rand.Next() % 10;
                        number = BigInteger.Parse(str);
                        SendString(io, number.ToString());
                        Console.WriteLine("   Число пользователя: " + number);

                        str = ReadString(io);
                        File.WriteAllText(@".\Client\" + userName, str);

                        break;
                    case "l":
                        Console.Write(ReadString(io));
                        userName = Console.ReadLine();
                        SendString(io, userName);

                        while (true)
                        {
                            string res = ReadString(io);
                            if (res == "   exit\n" + cpr) break;
                            Console.Write(res);
                            SendString(io, Console.ReadLine());
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("   Произошла ошибка {0}", e.Message);
            }
        }

        static string AddUser(string userName, BigInteger R)
        {
            string rec = "";
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] hash = md5Hash.ComputeHash(R.ToByteArray());
                rec += new BigInteger(OptMass(hash)) + "\n";

                for (int i = 1; i < 100; i++)
                {
                    hash = md5Hash.ComputeHash(hash);
                    rec += new BigInteger(OptMass(hash)) + "\n";
                }

                hash = md5Hash.ComputeHash(hash);

                File.WriteAllText(@".\Server\" + userName, new BigInteger(OptMass(hash)).ToString());
            }

            return rec;
        }

        static byte[] OptMass (byte[] mass)
        {
            byte[] ret = new byte[mass.Length + 1];
            for (int i = 0; i < mass.Length; i++)
            {
                ret[i] = mass[i];
            }
            return ret;
        }

        static byte[] ReOptMass(byte[] mass)
        {
            int md5Len = 16;

            if (mass.Length == md5Len)
            {
                return mass;
            }
            byte[] ret = new byte[md5Len];
            for (int i = 0; i < md5Len; i++)
            {
                ret[i] = mass[i];
            }
            return ret;
        }

        static string ReadString(NetworkStream io)
        {
            int length = io.Read(buffer, 0, buffer.Length);
            if (length == 0)
                return null;
            return encoding.GetString(buffer, 0, length);
        }

        static void SendString(NetworkStream io, string s)
        {
            var array = encoding.GetBytes(s);
            io.Write(array, 0, array.Length);
        }


        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("   Запуск программы в режиме сервера или клиента? [s/c]:");
                Console.Write(cpr);
                string ans = Console.ReadLine();

                switch (ans)
                {
                    case "s":
                        StartServer();
                        break;
                    case "c":
                        StartClient();
                        break;
                    case "exit":
                        return;
                    default:
                        Console.WriteLine("   Неверный параметр.");
                        break;
                }
            }
        }
    }
}
