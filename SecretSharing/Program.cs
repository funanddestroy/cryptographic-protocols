﻿using System;
using System.Numerics;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSharing
{
    class Program
    {
        static void Main(string[] args)
        {
            int m = 3;
            int n = 4;
            SecretSharing ss = new SecretSharing(m, n);

            string help = "init\t- ввести параметры работы (по умолчанию m = 3, n = 4);\n" +
                          "gs\t- сгенерировать секрет;\n" +
                          "ds\t- раскрыть секрет;\n" +
                          "exit\t- выход из программы.\n" +
                          "\n";
            Console.Write(help);


            string pr = "> ";

            while (true)
            {
                Console.Write(pr);
                string command = Console.ReadLine();

                switch (command)
                {
                    case "init":
                        Console.WriteLine("Введите m: ");
                        Console.Write(pr);
                        m = int.Parse(Console.ReadLine());
                        Console.WriteLine("Введите n: ");
                        Console.Write(pr);
                        n = int.Parse(Console.ReadLine());

                        Console.WriteLine("Параметры заданы");
                        break;

                    case "gs":
                        Console.WriteLine("Введите ключ сессии: ");
                        Console.Write(pr);
                        string sessionId = Console.ReadLine();
                        Console.WriteLine("Введите секрет: ");
                        Console.Write(pr);
                        BigInteger secret = BigInteger.Parse(Console.ReadLine());

                        BigInteger p = secret + 1;

                        while (!IsPrime(p))
                        {
                            p++;
                        }

                        ss = new SecretSharing(p, m, n);
                        ss.GenSecret(secret, sessionId);

                        Console.WriteLine("Секрет готов");
                        break;

                    case "ds":
                        Console.WriteLine("Введите количество ключей: ");
                        Console.Write(pr);
                        int keysCount = int.Parse(Console.ReadLine());
                        string[] keys = new string[keysCount];
                        Console.WriteLine("Введите пути ключей: ");
                        for (int i = 0; i < keysCount; i++)
                        {
                            Console.Write((i + 1) + " ключ ");
                            Console.Write(pr);
                            keys[i] = Console.ReadLine();
                        }

                        secret =  ss.DiscoverSecret(keys);

                        if (secret != -1) Console.WriteLine("Секрет: " + secret);

                        break;

                    case "exit":
                        return;
                }
            }
        }


        static bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
