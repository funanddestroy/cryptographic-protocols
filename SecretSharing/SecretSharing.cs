﻿using System;
using System.Numerics;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSharing
{
    class SecretSharing
    {
        private BigInteger p = BigInteger.Parse("11111111111111111111111");
        private int m;
        private int n;
        private string keysDir = "Keys";

        public SecretSharing(int m, int n)
        {
            this.m = m;
            this.n = n;
        }

        public SecretSharing(BigInteger p, int m, int n)
        {
            this.p = p;
            this.m = m;
            this.n = n;
        }
        
        public void GenSecret(BigInteger secret, string sessionId)
        {
            BigInteger[] d = new BigInteger[n];
            Queue<BigInteger> tmpq = new Queue<BigInteger>();
            tmpq.Enqueue(p * m);

            do
            {
                
                BigInteger iq = tmpq.Last() + 1;

                if (!IsPrime(iq))
                {
                    tmpq.Dequeue();
                }

                while (tmpq.Count < n)
                {
                    while (!IsPrime(iq))
                    {
                        iq++;
                    }
                    tmpq.Enqueue(iq++);
                }
                int i = 0;
                foreach (BigInteger bi in tmpq)
                {
                    d[i++] = bi;
                }

                tmpq.Dequeue();

            } while (!testNumb(d));

            BigInteger pr = p * 100;
            BigInteger r = 0;

            Random rand = new Random();
            while (r < d[n - 1] * 2)
            {
                string str = "";
                for (int j = 0; j < pr.ToString().Length; j++)
                    str += rand.Next() % 10;

                r = BigInteger.Parse(str) % pr;
            }

            BigInteger secshtr = secret + r * p;

            if (!Directory.Exists(keysDir))
            {
                Directory.CreateDirectory(keysDir);
            }

            for (int i = 0; i < n; i++)
            {
                BigInteger k = secshtr % d[i];
                StringBuilder key = new StringBuilder();
                key.Append(p + "\n");
                key.Append(d[i] + "\n");
                key.Append(k);

                File.WriteAllText(keysDir + "\\" + sessionId + (i + 1), key.ToString());
            }
        }

        public BigInteger DiscoverSecret(string[] keys)
        {
            if (keys.Length < m)
            {
                Console.WriteLine("Недостаточно ключей");
                return -1;
            }
            
            BigInteger[] d = new BigInteger[keys.Length];
            BigInteger[] k = new BigInteger[keys.Length];
            int i = 0;
            foreach (string key in keys)
            {
                if (!File.Exists(key))
                {
                    Console.WriteLine("Файла " + key + " не существует");
                    return -1;
                }
                string[] lines = File.ReadAllLines(key);
                p = BigInteger.Parse(lines[0]);
                d[i] = BigInteger.Parse(lines[1]);
                k[i++] = BigInteger.Parse(lines[2]);
            }

            BigInteger M = 1;
            foreach (BigInteger a in d)
            {
                M *= a;
            }

            BigInteger[] Mi = new BigInteger[d.Length];
            for (i = 0; i < d.Length; i++)
            {
                Mi[i] = M / d[i];
            }

            BigInteger[] inverseMi = new BigInteger[d.Length];
            for (i = 0; i < Mi.Length; i++)
            {
                inverseMi[i] = Extended_GCD(Mi[i], d[i])[1];
            }

            BigInteger secret = 0;
            for (i = 0; i < d.Length; i++)
            {
                secret += k[i] * Mi[i] * inverseMi[i]; 
            }
            secret = (secret % M + M) % M;

            return (secret % p + p) % p;
        }

        private bool testNumb(BigInteger[] d)
        {
            BigInteger left = 1;
            BigInteger right = 1;
            for (int i = 0; i < m; i++)
            {
                left *= d[i];
            }
            right *= p;
            for (int i = n - m + 1; i < n; i++)
            {
                right *= d[i];
            }

            return (left > right);
        }

        private bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        private BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B) 
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }
    }
}
